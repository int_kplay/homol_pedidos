unit Main_Service;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs, Registry,
  Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef,
  FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet,
  Xml.xmldom, Xml.XMLIntf, Xml.XMLDoc, ComObj, MSXML, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdHTTP, frxClass, IdMessage, IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP;

type
  TAttPedidosHomol = class(TService)
    TimerPedidos: TTimer;
    FDConnection1: TFDConnection;
    FDQuery1: TFDQuery;
    XMLDocument1: TXMLDocument;
    FDQuery2: TFDQuery;
    IdHTTP1: TIdHTTP;
    frxReport1: TfrxReport;
    IdSMTP: TIdSMTP;
    IdMessage: TIdMessage;
    procedure ServiceAfterInstall(Sender: TService);
    procedure ServiceAfterUninstall(Sender: TService);
    procedure ServiceBeforeInstall(Sender: TService);
    procedure ServiceBeforeUninstall(Sender: TService);
    procedure ServiceContinue(Sender: TService; var Continued: Boolean);
    procedure ServiceCreate(Sender: TObject);
    procedure ServiceDestroy(Sender: TObject);
    procedure ServiceExecute(Sender: TService);
    procedure ServicePause(Sender: TService; var Paused: Boolean);
    procedure ServiceShutdown(Sender: TService);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure TimerPedidosTimer(Sender: TObject);
  private
    { Private declarations }
  public
    function GetServiceController: TServiceController; override;
    function EnviaEmailTimeOut(Pedido: String; Status : string; Erro: String): string;
    function ConvertStringEmail(Texto: String): string;
    { Public declarations }
  end;

var
  AttPedidosHomol: TAttPedidosHomol;
  lResponse : TStringStream;
  Envio, Retorno : string;
  contadorImportados, contadorNImportados : Integer;

implementation

{$R *.dfm}

function TAttPedidosHomol.ConvertStringEmail(Texto: String): string;
{==============================================================================}
{ Autor: Raphael Moreira da Silva                                              }
{ Data: 13/02/2017                                                             }
{ Nome da Fun��o: ConvertStringEmail                                           }
{------------------------------------------------------------------------------}
{ Objetivos:                                                                   }
{     Todas as Strings que ser�o enviadas por email devem passar pela convers�o}
{  para serem exibidas corretamente.                                           }
{ Parametros:                                                                  }
{     Texto: Deve ser a string escolhida para convers�o.                       }
{ Retornos:                                                                    }
{     String convertida pronta para ser exibida no email.                      }
{==============================================================================}
var
 Texto4Convert : string;
begin

     Texto4Convert := Texto;

     Texto4Convert  := StringReplace(Texto4Convert, '�', '&Aacute;', [rfReplaceAll]);  Texto4Convert  := StringReplace(Texto4Convert, '�', '&aacute;', [rfReplaceAll]);
     Texto4Convert  := StringReplace(Texto4Convert, '�', '&Acirc;',  [rfReplaceAll]);  Texto4Convert  := StringReplace(Texto4Convert, '�', '&acirc;',  [rfReplaceAll]);
     Texto4Convert  := StringReplace(Texto4Convert, '�', '&Atilde;', [rfReplaceAll]);  Texto4Convert  := StringReplace(Texto4Convert, '�', '&atilde;', [rfReplaceAll]);
     Texto4Convert  := StringReplace(Texto4Convert, '�', '&Agrave;', [rfReplaceAll]);  Texto4Convert  := StringReplace(Texto4Convert, '�', '&agrave;', [rfReplaceAll]);

     Texto4Convert  := StringReplace(Texto4Convert, '�', '&Eacute;', [rfReplaceAll]);  Texto4Convert  := StringReplace(Texto4Convert, '�', '&eacute;', [rfReplaceAll]);
     Texto4Convert  := StringReplace(Texto4Convert, '�', '&Ecirc;',  [rfReplaceAll]);  Texto4Convert  := StringReplace(Texto4Convert, '�', '&ecirc;',  [rfReplaceAll]);
     Texto4Convert  := StringReplace(Texto4Convert, '�', '&Etilde;', [rfReplaceAll]);  Texto4Convert  := StringReplace(Texto4Convert, '�', '&etilde;', [rfReplaceAll]);

     Texto4Convert  := StringReplace(Texto4Convert, '�', '&Iacute;', [rfReplaceAll]);  Texto4Convert  := StringReplace(Texto4Convert, '�', '&iacute;', [rfReplaceAll]);
     Texto4Convert  := StringReplace(Texto4Convert, '�', '&Icirc;',  [rfReplaceAll]);  Texto4Convert  := StringReplace(Texto4Convert, '�', '&icirc;',  [rfReplaceAll]);
     Texto4Convert  := StringReplace(Texto4Convert, '�', '&Itilde;', [rfReplaceAll]);  Texto4Convert  := StringReplace(Texto4Convert, '�', '&itilde;', [rfReplaceAll]);

     Texto4Convert  := StringReplace(Texto4Convert, '�', '&Oacute;', [rfReplaceAll]);  Texto4Convert  := StringReplace(Texto4Convert, '�', '&oacute;', [rfReplaceAll]);
     Texto4Convert  := StringReplace(Texto4Convert, '�', '&Ocirc;',  [rfReplaceAll]);  Texto4Convert  := StringReplace(Texto4Convert, '�', '&ocirc;',  [rfReplaceAll]);
     Texto4Convert  := StringReplace(Texto4Convert, '�', '&Otilde;', [rfReplaceAll]);  Texto4Convert  := StringReplace(Texto4Convert, '�', '&otilde;', [rfReplaceAll]);
     Texto4Convert  := StringReplace(Texto4Convert, '�', '&Ograve;', [rfReplaceAll]);  Texto4Convert  := StringReplace(Texto4Convert, '�', '&ograve;', [rfReplaceAll]);

     Texto4Convert  := StringReplace(Texto4Convert, '�', '&Uacute;', [rfReplaceAll]);  Texto4Convert  := StringReplace(Texto4Convert, '�', '&uacute;', [rfReplaceAll]);
     Texto4Convert  := StringReplace(Texto4Convert, '�', '&Ucirc;',  [rfReplaceAll]);  Texto4Convert  := StringReplace(Texto4Convert, '�', '&ucirc;',  [rfReplaceAll]);
     Texto4Convert  := StringReplace(Texto4Convert, '�', '&Utilde;', [rfReplaceAll]);  Texto4Convert  := StringReplace(Texto4Convert, '�', '&utilde;', [rfReplaceAll]);

     Texto4Convert  := StringReplace(Texto4Convert, '�', '&Ccedil;', [rfReplaceAll]);  Texto4Convert  := StringReplace(Texto4Convert, '�', '&ccedil;', [rfReplaceAll]);

     Result := Texto4Convert;

end;

function TAttPedidosHomol.EnviaEmailTimeOut(Pedido: String; Status : string; Erro: String): string;
{==============================================================================}
{ Autor: Raphael Moreira da Silva                                              }
{ Data: 13/02/2017                                                             }
{ Nome da Fun��o: EnviaEmailTimeOut                                            }
{------------------------------------------------------------------------------}
{ Objetivos:                                                                   }
{     Configura o SMTP e envia o email informando falhas na conex�o com o      }
{     restfull ocpedidos                                                       }
{ Parametros:                                                                  }
{     Pedido ->  Pedido atual em que a integra��o falhou.                      }
{     Status ->  Status atual na integra�ao desse pedido.                      }
{     Erro   ->  Erro apresentado no momento da tentativa de integra��o        }
{ Retornos:                                                                    }
{     Email enviado com sucesso                                                }
{==============================================================================}
var
 assuntoEmail : string;
 AutenticouSMTP: boolean;
begin

     assuntoEmail := 'Erro conex�o [pedido_de_venda] Homologa��o ';

     AttPedidosHomol.IdMessage.CharSet      := 'ISO-8859-1';
     AttPedidosHomol.IdMessage.Encoding     := meMIME;
     AttPedidosHomol.IdMessage.ContentType  :='text/html';
     AttPedidosHomol.IdMessage.From.Name    :='Integra��o Pedidos Homologa��o';
     AttPedidosHomol.IdMessage.From.Address :='naoresponda@kplay.com.br';
     AttPedidosHomol.IdMessage.Priority     := mpNormal;
     AttPedidosHomol.IdMessage.Subject      := assuntoEmail;
     AttPedidosHomol.IdMessage.Date         := Now;

     //Add Destinat�rio(s)

     AttPedidosHomol.IdMessage.Recipients.Add;
     AttPedidosHomol.IdMessage.Recipients.EMailAddresses := 'raphael.moreira@kplay.com.br; lucas@xcompany.com.br; fabricio@xcompany.com.br; marcelo@xcompany.com.br; suporte.oceanic@xcompany.com.br';


     //IdMessage.CCList.EMailAddresses   :='raphael.moreira@kplay.com.br';  // Sem emails em c�pia ()
     //IdMessage.BccList.EMailAddresses  :='raphael.moreira@kplay.com.br';  // Sem emails em c�pia oculta ()

       // corpo
      AttPedidosHomol.IdMessage.Body.Clear;
      AttPedidosHomol.IdMessage.Body.Add('<html>');
      AttPedidosHomol.IdMessage.Body.Add('    <body>');

      AttPedidosHomol.IdMessage.Body.Add('        <font face="Courier" size="5"> Falha em conectar/obter resposta</font> <br />');
      AttPedidosHomol.IdMessage.Body.Add('        <hr>');

      AttPedidosHomol.IdMessage.Body.Add('        <br />');

      AttPedidosHomol.IdMessage.Body.Add('        <font face="Courier" size="3">#Pedido: '+ ConvertStringEmail(Pedido)+'</font> <br />');
      AttPedidosHomol.IdMessage.Body.Add('        <font face="Courier" size="3">#Status: '+ ConvertStringEmail(Status)+'</font> <br />');
      AttPedidosHomol.IdMessage.Body.Add('        <font face="Courier" size="3">#Erro: '  + ConvertStringEmail(Erro)+'</font> <br />');

      AttPedidosHomol.IdMessage.Body.Add('    </body>');
      AttPedidosHomol.IdMessage.Body.Add('</html>');

      AttPedidosHomol.IdSMTP.IOHandler := nil;
      AutenticouSMTP := False;

       //Preparando Servidor...

       AttPedidosHomol.IdSMTP.Host     := 'smtp.xcompany.com.br';
       AttPedidosHomol.IdSMTP.Authtype := satDefault;
       AttPedidosHomol.IdSMTP.Port     := 587;
       AttPedidosHomol.IdSMTP.Username := 'naoresponda@kplay.com.br';
       AttPedidosHomol.IdSMTP.Password := 'kplay2013@99';

       // efetua a conexao ao servidor SMTP
      if not AttPedidosHomol.IdSMTP.Connected then
      //try
      begin
          AttPedidosHomol.IdSMTP.Connect; // conecta ao SMTP
          AutenticouSMTP := AttPedidosHomol.IdSMTP.Authenticate // efetua a atenticacao e retorna o resultado para a vari�vel
      end;

      // se a conexao foi bem sucedida, envia a mensagem
      if AutenticouSMTP and AttPedidosHomol.IdSMTP.Connected then
      begin
          AttPedidosHomol.IdSMTP.Send(AttPedidosHomol.IdMessage);

      end;
      // depois de tudo pronto, desconecta do servidor SMTP
      if AttPedidosHomol.IdSMTP.Connected then AttPedidosHomol.IdSMTP.Disconnect;


     Result := 'Enviado com sucesso';

end;

procedure doSaveLog(Msg: string);
var
 ioLista: TStringList;
begin

    try

          ioLista:= TStringList.Create;

          try
               if FileExists('c:/Integracao/att_pedido_homologa.log') then
               ioLista.LoadFromFile('c:/Integracao/att_pedido_homologa.log');
               ioLista.Add(TimeToStr(now) + ':' + Msg);


          except
               on e: exception do
               ioLista.Add(TimeToStr(now) + ' Erro : ' + E.Message);

          end;

     finally

          ioLista.SaveToFile('c:/Integracao/att_pedido_homologa.log');
          ioLista.Free;

     end;
end;

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  AttPedidosHomol.Controller(CtrlCode);
end;

function TAttPedidosHomol.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TAttPedidosHomol.ServiceAfterInstall(Sender: TService);
var
  Reg: TRegistry;
begin
     doSaveLog('Service After Install');
     contadorImportados  := 1;
     contadorNImportados := 1;

  Reg := TRegistry.Create(KEY_READ or KEY_WRITE);
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKey('\SYSTEM\CurrentControlSet\Services\' + Name, false) then
    begin
      Reg.WriteString('Description', 'Integra��o pedidos Oceanic Homologa��o. Desenvolvido por: Raphael Moreira');
      Reg.CloseKey;
    end;
  finally
    Reg.Free;
  end;
end;

procedure TAttPedidosHomol.ServiceAfterUninstall(Sender: TService);
begin
     doSaveLog('Service After Uninstall');
end;

procedure TAttPedidosHomol.ServiceBeforeInstall(Sender: TService);
begin
     doSaveLog('Service Before Install');
end;

procedure TAttPedidosHomol.ServiceBeforeUninstall(Sender: TService);
begin
     doSaveLog('Service Before Uninstall');
end;

procedure TAttPedidosHomol.ServiceContinue(Sender: TService; var Continued: Boolean);
begin
     doSaveLog('Service Continue');

end;

procedure TAttPedidosHomol.ServiceCreate(Sender: TObject);
begin
     doSaveLog('Service Create');
end;

procedure TAttPedidosHomol.ServiceDestroy(Sender: TObject);
begin
     doSaveLog('Service Destroy');
end;

procedure TAttPedidosHomol.ServiceExecute(Sender: TService);
begin
     doSaveLog('Service Execute');
     while not self.Terminated do
     ServiceThread.ProcessRequests(true);
end;

procedure TAttPedidosHomol.ServicePause(Sender: TService; var Paused: Boolean);
begin
     doSaveLog('Service Pause');
end;

procedure TAttPedidosHomol.ServiceShutdown(Sender: TService);
begin
     doSaveLog('Service Shutdown');
end;

procedure TAttPedidosHomol.ServiceStart(Sender: TService; var Started: Boolean);
begin
     doSaveLog('Service Start');
end;

procedure TAttPedidosHomol.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
     doSaveLog('Service Stop');
end;

procedure TAttPedidosHomol.TimerPedidosTimer(Sender: TObject);
var
  oXMLHTTP, oXMLHTTP_existe: OleVariant;
  xml, xml_existe: IXMLDOMDocument;
  node, node_existe: IXMLDomNode;
  nodes_row, nodes_row_existe: IXMLDomNodeList;
  i, i_existe: Integer;
  pedido, integrado, mensagem, interacao, obs, ok, pedido_tiraerro: string;
  timeout, no_return, verifica, strTime, statusDescri, local: string;
  XML_R: IXMLDocument;
  h, m, s, ms: Word;
  nome, valor, data : string;
  status_01, data_01, hora_01, log, horario,mensagemErro,dataRetorno : string;
  eConexao : string;
begin

     { Integra��o dos pedidos Oceanic com o ocpedidos. }
     { Raphael Moreira, 04/11/2016, fun��o atualizada 22/02/2017 }

     interacao := 'Xml de Envio - Pedido';
     obs       := 'Falha na integracao - erro no retorno';
     ok        := 'Importado com sucesso';
     timeout   := 'Timeout';
     no_return := 'Sem retorno';
     mensagemErro := '';
     dataRetorno  := '';

     decodetime(now, h, m, s, ms);
     strTime := intToStr(h) + ':' + intToStr(m);
     statusDescri := 'PEDIDO INTEGRADO';

     try

        FDQuery1.Open;
        verifica := 'sim'; // Achou pedidos pendentes de integra��o ;

     except on E:Exception do
            begin

              doSaveLog('Erro ao iniciar integra��o da fila');
              verifica := 'n�o';

            end;
     end;

     if verifica = 'sim' then
     begin
          while not FDQuery1.Eof do          // Loop para enviar todos os pedidos que encontrou na fila ;
          begin

               try
                    pedido_tiraerro := FDQuery1.FieldByName('pedido').AsString;
                    FDQuery2.Close;
                    FDQuery2.SQL.Text := 'exec integracao_pedido_sp :pedido';          // Procedure que monta o XML para enviar ao ocpedidos ;
                    FDQuery2.ParamByName('pedido').AsString := pedido_tiraerro;
                    FDQuery2.Open;

               except on E: Exception do
                      begin

                         doSaveLog('Erro ao gerar xml' + pedido + ' Erro - ' + e.Message);
                         FDQuery1.Next;
                         Continue;

                      end;
               end;

               try
                    XML_R     := TXMLDocument.Create('');
                    lResponse := TStringStream.Create('');

                    // Capturando XML montado e informa��es adicionais sobre o pedido ;

                    XML_R.LoadFromXML(FDQuery2.FieldByName('xml').AsString);

                    local  := FDQuery2.FieldByName('local').AsString;
                    Envio  := FDQuery2.FieldByName('xml').AsString;
                    nome   := FDQuery2.FieldByName('nome').AsString;
                    valor  := FDQuery2.FieldByName('valor').AsString;
                    data   := FDQuery2.FieldByName('data').AsString;

                    XML_R.SaveToFile('C:/Integracao/pedido.xml');     // Salvando XML localmente na pasta (direitos de admin)

               except on E: Exception do
                      begin

                         doSaveLog('Erro ao salvar xml' + pedido);
                         FDQuery1.Next;
                         Continue;

                      end;
               end;

               try

                    oXMLHTTP := CreateOleObject('MSXML2.ServerXMLHTTP');
                    oXMLHTTP.setOption(2, 'SXH_SERVER_CERT_IGNORE_ALL_SERVER_ERRORS');
                    oXMLHTTP.open('POST', 'http://201.25.131.174:8091/rest/pedido_de_venda', False);
                    oXMLHTTP.setRequestHeader('Authorization', 'Basic ' + 'cmVzdHVzZXI6MWludGVncmF0b3RxYTI=');
                    oXMLHTTP.setRequestHeader('Content-Type', 'application/xml');
                    oXMLHTTP.setRequestHeader('User-Agent', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)');
                    oXMLHTTP.setRequestHeader('Cache-Control', 'no-cache');
                    oXMLHTTP.setTimeouts (300000, 300000, 300000, 300000);            // Timeout 5 minutos (a pedido de Marcio - Totvs) ;
                    oXMLHTTP.send(FDQuery2.FieldByName('xml').AsString);

               except on E: EOleException do

                    begin

                         // Caso aconte�a falhas na conex�o com o rest enviar email avisando () ;

                         doSaveLog('Erro conex�o: ' + e.Message);
                         FDConnection1.ExecSQL('INSERT INTO int_logerro (pedido, mensagem, xml_retorno, data) VALUES (:pedido, :mensagem, :retorno, getdate())', [pedido_tiraerro, timeout, no_return]);

                         if (e.Message = 'O tempo limite da opera��o foi atingido') or (e.Message = 'The operation timed out') then
                         begin

                              // TIMEOUT (5 minutos)

                              eConexao := EnviaEmailTimeOut(pedido_tiraerro, '300 segundos sem resposta sobre status da integra��o', e.Message);
                              Sleep(600000);  // Tentar novamente a cada 10 minutos ;

                         end;

                         if (e.Message = 'Uma conex�o com o servidor n�o p�de ser estabelecida') or (e.Message = 'A connection with the server could not be established') then
                         begin

                              // SEM CONEX�O ;

                              eConexao := EnviaEmailTimeOut(pedido_tiraerro, 'Sem conex�o', e.Message);
                              Sleep(600000);  // Tentar novamente a cada 10 minutos ;

                         end;

                         Continue;

                    end;
               end;

               xml := CreateOleObject('Microsoft.XMLDOM') as IXMLDOMDocument;
               xml.async := False;
               xml.loadXML(string(oXMLHTTP.responsetext));         // Capturando retorno do ocpedidos ;

               if xml.parseError.errorCode <> 0 then
               begin
                    // Erros aqui:
                         // A conex�o com o servidor foi interrompida de modo anormal : ???????
                         // Retorno em JSON que o server esta em manuten��o ou caiu ;
                         // XML de retorno com caracteres inv�lidos ou o pr�prio xml inv�lido (verificar em: http://codebeautify.org/xmlvalidator) ;
                              // Caso for o xml inv�lido avisar o Marcio/Fl�vio da Totvs/Oceanic ;

                    doSaveLog('ERRO NO PARSE, ENTRAR EM CONTATO COM A TOTVS/ xml mal formatado:' + xml.parseError.reason + ' XML -' + oXMLHTTP.responsetext + ' Pedido -' + pedido_tiraerro);

                    if oXMLHTTP.responsetext = '{"message":"The server is currently unavailable (because it is overloaded or down for maintenance). Generally, this is a temporary state."}' then
                    begin

                         // SERVIDOR EM MANUTEN��O SEM AVISO PR�VIO () ;

                         eConexao := EnviaEmailTimeOut(pedido_tiraerro, 'Manuten��o sem aviso pr�vio', oXMLHTTP.responsetext);
                         Sleep(600000);

                    end;

                    raise Exception.Create('XML Load error:' + xml.parseError.reason);
               end;

               // Pegando nodes pela numera��o ;
               nodes_row := xml.selectNodes('/results/result/parameter');
               for i := 0 to nodes_row.length - 1 do
               begin

                    node := nodes_row.item[i];


                    {

                         -- RETORNO OCPEDIDOS --

                         <?xml version="1.0" encoding="UTF-8"?>
                         <results>
                              <result>
                                 0  <parameter name="local"      value="010101"/>
                                 1  <parameter name="code"       value="0000900227"/>
                                 2  <parameter name="integrated" value="true"/>
                                 3  <parameter name="message"    value="Pedido integrado"/>
                              </result>
                         </results>

                    }

                    if i = 1 then
                    begin

                         pedido := Format('%s',[String(node.attributes.getNamedItem('value').Text)]);

                    end;

                    if i = 2 then
                    begin

                         integrado := Format('%s',[String(node.attributes.getNamedItem('value').Text)]);

                    end;

                    if i = 3 then
                    begin

                         mensagem := Format('%s',[String(node.attributes.getNamedItem('value').Text)]);

                    end;
               end;

               if integrado = 'false' then
               begin

                    if pedido = '' then
                    begin
                         pedido := pedido_tiraerro;          // Evitando duplica��o de registros na int_Painel ;
                    end;

                    // Se o pedido ja existir ;
                         // Motivos:
                              { Bateu o timeout de 5 minutos sem resposta do proteus, ou foi enviado via postman por algu�m da oceanic }

                    if mensagem = 'pedido ja existe' then
                    begin

                         // Se o retorno foi que o pedido ja existe ent�o o primeiro status (PEDIDO RECEBIDO) tem que ser retornado pelo ocstatuspv

                         try

                              oXMLHTTP_existe := CreateOleObject('MSXML2.ServerXMLHTTP');
                              oXMLHTTP_existe.setOption(2, 'SXH_SERVER_CERT_IGNORE_ALL_SERVER_ERRORS');
                              oXMLHTTP_existe.open('GET', 'http://201.25.131.174:8091/rest/consulta_status_de_entrega_pedido_de_venda/'+pedido+'/'+local+'', False);    // Verificar status em produ��o ;
                              oXMLHTTP_existe.setRequestHeader('Authorization', 'Basic ' + 'cmVzdHVzZXI6MWludGVncmF0b3RxYTI=');
                              oXMLHTTP_existe.setRequestHeader('Content-Type', 'application/xml');
                              oXMLHTTP_existe.setRequestHeader('User-Agent', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)');
                              oXMLHTTP_existe.setRequestHeader('Cache-Control', 'no-cache');
                              oXMLHTTP_existe.setTimeouts (60000, 60000, 60000, 60000);     // Timeout ocstatuspv de 1 minuto ;
                              oXMLHTTP_existe.send(EmptyStr);

                         except on E: EOleException do
                                begin

                                   FDQuery1.Next;
                                   doSaveLog('Erro ao verificar status do pedido que ja existe ' + pedido + ' Erro -' + e.Message);
                                   Continue;

                                end;
                         end;

                         xml_existe := CreateOleObject('Microsoft.XMLDOM') as IXMLDOMDocument;
                         xml_existe.async := False;
                         xml_existe.loadxml(string(oXMLHTTP_existe.responsetext));

                         if xml_existe.parseError.errorCode <> 0 then
                         begin
                              doSaveLog('ERRO NO PARSE, ENTRAR EM CONTATO COM A TOTVS/ xml mal formatado:' + xml.parseError.reason + ' XML -' + oXMLHTTP_existe.responsetext + ' Pedido -' + pedido_tiraerro);
                              raise Exception.Create('XML Load error:' + xml_existe.parseError.reason);
                         end;

                         nodes_row_existe := xml_existe.selectNodes('/result/parameter');

                         for i_existe := 0 to nodes_row_existe.length - 1 do
                         begin

                              node_existe := nodes_row_existe.item[i_existe];

                              // Pegando Status Dispon�veis ;

                              if i_existe = 2 then status_01 := Format('%s',[String(node_existe.attributes.getNamedItem('value').Text)]);
                              if i_existe = 3 then data_01   := Format('%s',[String(node_existe.attributes.getNamedItem('value').Text)]);
                              if i_existe = 4 then hora_01   := Format('%s',[String(node_existe.attributes.getNamedItem('value').Text)]);

                              // Pegando somente a numera��o correspondente ao PEDIDO RECEBIDO ;

                         end;

                         doSaveLog('8');
                         if status_01 = 'PEDIDO RECEBIDO' then
                         begin

                              // Se encontrou o primeiro status, atualiza o pedido como se tivesse integrado normalmente e joga na int_painel que ele ja existia ;

                              try

                                   log := 'Pedido ja existe, atualizado - ' + pedido;
                                   horario := TimeToStr(now);
                                   if Envio.Length > 4000 then Envio := 'XML Excedeu limite';
                                   doSaveLog('exec integracao_geraPedido_sp '+pedido+', '+status_01+', '+data_01+', '+hora_01+', '+local+','+log+','+horario+','+Envio+', '+mensagemErro+',2');

                                   FDConnection1.ExecSQL('exec integracao_geraPedido_sp :pedido, :status, :dataRetorno, :horaRetorno, :local, :mensagemLog, :horarioLog, :xmlEnviado, :mensagemErro,2', [pedido, status_01, data_01, hora_01, local, log, horario, Envio,mensagemErro]);


                              except on E: Exception do
                                     begin

                                        FDConnection1.ExecSQL('UPDATE ped_pedidototal SET ws = 2 where pediCodigo = :pedido', [pedido_tiraerro]);
                                        FDConnection1.ExecSQL('INSERT INTO int_logerro (pedido, mensagem, xml_retorno, data) VALUES (:pedido, Erro XML, :retorno, getdate())', [pedido_tiraerro, no_return]);
                                        doSaveLog('Erro ao salvar novo status' + pedido);
                                        FDQuery1.Next;
                                        Continue;

                                     end;
                              end;
                         end;
                    end
                    else
                    begin

                         // Se n�o encontrou PEDIDO RECEBIDO, passa o WS para 2 e aponta o erro na int_painel ;
                         try

                              log := 'Pedido n�o importado - ' + pedido + ' Erro - ' + mensagem;
                              horario := TimeToStr(now);
                              // XML � maior que o limite do campo no SQL Server  ;
                                   // Se precisar visualizar o xml a proc que monta tem um par�metro a mais apenas para visualiza��o ;
                              if Envio.Length > 4000 then Envio := 'XML Excedeu limite';
                              FDConnection1.ExecSQL('exec integracao_geraPedido_sp :pedido, :status, :dataRetorno, :horaRetorno, :local, :mensagemLog, :horarioLog, :xmlEnviado, :mensagemErro,1', [pedido, statusDescri, dataRetorno, strTime, local, log, horario, Envio,mensagem]);

                              doSaveLog('Pedido n�o importado - ' + pedido + ' Erro - ' + mensagem);

                         except on E: Exception do
                                begin

                                   FDConnection1.ExecSQL('UPDATE ped_pedidototal SET ws = 2 where pediCodigo = :pedido', [pedido_tiraerro]);
                                   FDConnection1.ExecSQL('INSERT INTO int_logerro (pedido, mensagem, xml_retorno, data) VALUES (:pedido, :mensagem, :retorno, getdate())', [pedido_tiraerro, timeout, no_return]);
                                   FDQuery1.Next;
                                   doSaveLog('Erro ao salvar pedido nao importado' + pedido);
                                   Continue;

                                end;
                         end;
                    end;

               end
               else
               begin

                    // Pedido integrado normalmente, grava na int_painel os dados sobre o pedido juntamente com o xml enviado ;

                    try

                         log := 'Pedido importado - ' + pedido;
                         horario := TimeToStr(now);
                         if Envio.Length > 4000 then Envio := 'XML Excedeu limite';
                         FDConnection1.ExecSQL('exec integracao_geraPedido_sp :pedido, :status, :dataRetorno, :horaRetorno, :local, :mensagemLog, :horarioLog, :xmlEnviado, :mensagemErro,0', [pedido, statusDescri, dataRetorno, strTime, local, log, horario, Envio,mensagemErro]);
                         doSaveLog('Pedido importado - ' + pedido);

                    except on E: Exception do
                           begin

                              FDQuery1.Next;
                              doSaveLog('Erro ao salvar pedido importado');
                              Continue;
                           end;
                    end;
               end;

               FDQuery1.Next;
          end;

          FDQuery1.Close;
     end;
end;

end.
